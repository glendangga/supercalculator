#Muhammad Glend Angga Dwitama
#1806191396

from tkinter import *
from math import *
from idlelib.ToolTip import *
import struct

class Calc:
    def __init__(self):
        #make window
        window = Tk()
        self.window = window
        #lock the window ( cant be resized)
        self.window.title("Super Calculator[by M Glend Angga D]")
        self.window.minsize(width=375, height =475)
        self.window.maxsize(width=375, height =475)
        frame1 = Frame(window)
        
        #make entry for user's input
        self.entry = Entry (frame1, relief=RIDGE, borderwidth=5, width=27\
                               , bg='white', font=('Helvetica', 18))
        self.entry.grid(row=0, column=0, columnspan=5)
        self.memory = '0'   #for operand M+, M-, MR, MC
        self.operator = ''  #first operand and equation container
        self.startOfNextOperand = True
        buttons = [['Clr', 'MC', 'M+', 'M-', 'MR'],\
                   ['d', 'e', 'f', '+', '-'],\
                   ['a', 'b', 'c', '/', '*'],\
                   ['7', '8', '9', '**', '√'],\
                   ['4', '5', '6', 'sin', 'cos'],\
                   ['1', '2', '3', 'tan', 'ln'],\
                   ['0', '.', '±', '~', '2C'],\
                   ['x', 'o', '^', '|', '&'],\
                   ['π', 'int', 'rad', '//', 'exp'],\
                   ['→IEEE','←IEEE', 'asin', 'acos', 'atan'],\
                   ['bin', 'hex', 'oct', '%', '=']]

        #Array of tooltips available when the cursor is on a button
        ToolTips = [['Clear', 'Memory Clear', 'Memory Add', 'Memory Subtract', 'Memory Recall'],
                    ['Letter d', 'Letter e/Euler Number', 'Letter f', 'Add', 'Subtract'],
		    ['Letter a', 'Letter b', 'Letter c', 'Float Divide', 'Multiply' ],
		    ['Seven', 'Eight', 'Nine', 'Power by n', 'Square Root'],
		    ['Four', 'Five', 'Six', 'sin(radians)', 'cos(radians)'],
		    ['One', 'Two', 'Three', 'tan(radians)', 'natural log'],
		    ['Zero', 'Decimal point', 'Toggle +- sign', 'Bitwise complement', "32-bit 2's Complement"],
		    ['Letter x', 'Letter o', 'Bitwise XOR', 'Bitwise OR', 'Bitwise AND'],
		    ['Pi', 'Change to Integer', 'Change to radians', 'Integer divide', 'Power of E(2,718...)'],
                    ['decimal to 64-bit IEEE 754 representation (in hex)', '64-bit IEEE 754 representation (in hex) to decimal','Return the arc sine of x, in radians.','Return the arc cosine of x, in radians.','Return the arc tangent of x, in radians.'],
		    ['Change to binary', 'Change to hexadecimal', 'Change to octal', 'Modulo', 'Compute to decimal']]

        #Looping for each button and tool tip in the row and column
        for r in range (11):
            for c in range (5):
                '''
                function cmd() is defined so that when it is called without
                an input argument, it executes self.click(buttons[r][c])
                '''
                def cmd (x=buttons[r][c]):
                    self.click(x)
                
                b = Button (frame1, text=buttons[r][c], width=6, relief=RAISED,\
                            bg='green', font=('Arial', 15), command = cmd)
                b.grid(row = r+1, column=c)
                tip = ToolTip (b, ToolTips[r] [c])
                
        frame1.pack()
        window.mainloop()

    #main function, when a button in the app is clicked
    def click(self, key):
        #action called when the button '=' is clicked
        if key == '=':
            try:
                result = eval(self.operator + self.entry.get()) #string
                self.entry.delete(0) # emptying the bar for displaying only the result
                self.entry.insert(END, result) #display the result of operation
                self.operator='' #remove the last operator
            except: #Error Handling
                self.entry.delete(0,END) # emptying the bar for displaying only the result
                self.entry.insert(END, 'Error') #display the result of operation
            self.startOfNextOperand = True #next operation

        #standard python math operations
        elif key in ['+', '-', '*', '/' , '//' , '%', '**', '|', '^']:
            self.operator += '(' + self.entry.get() + ')'
            self.operator += key
            self.startOfNextOperand = True

        #Clear (Clears the input pane)
        elif key == 'Clr' :
            self.entry.delete(0,END)

        #toggle ± sign
        elif key == '±':
            try:
                self.startOfNextOperand = True
                if self.entry.get()[0] == '-':
                    self.entry.delete(0)
                else:
                    self.entry.insert(0,'-')
            except IndexError: #if nothing to +- then do nothing
                pass

        #Square root
        elif key == '√':
            try:
                self.startOfNextOperand = True
                temp_res = sqrt(float(self.entry.get()))
                self.entry.delete(0,END)
                self.entry.insert(END, temp_res)
            except:
                self.entry.delete(0,END)
                self.entry.insert(END, 'Error')

        #Standard Trigonometry operations + exp (Powered by Euler Number)
        elif key in ['sin', 'cos', 'tan', 'exp']:
            self.startOfNextOperand = True
            try:
                temp_res = eval('{} (float({}))'.format(key, self.entry.get()))
                self.entry.delete(0,END)
                self.entry.insert(END, temp_res)
            except:
                self.entry.delete(0, END)
                self.entry.insert(END, 'Error')

        #Standard Inverse Trigonometry operations + exp (Power by Euler Number)
        elif key in ['asin', 'acos','atan']:
            try:
                temp_res = eval('{} ({})'.format (key,self.entry.get()))
                self.entry.delete(0,END)
                self.entry.insert(END, temp_res)
            except:
                self.entry.delete(0,END)
                self.entry.insert(END, 'Error')       

        #Pi (3.14.....)     
        elif key == 'π':
            self.startOfNextOperand = True
            self.entry.delete(0,END)
            self.entry.insert(END, pi)

        #Natural log
        elif key == 'ln':
            self.startOfNextOperand = True
            temp_res = log(float(self.entry.get()))
            self.entry.delete(0,END)
            self.entry.insert(END, temp_res)

        #Bitwise complement
        elif key == '~':
            try:
                self.startOfNextOperand = True
                temp_res = eval('~{}'.format(self.entry.get())) #calculate the operation
                self.entry.delete(0,END)
                self.entry.insert(END, temp_res)
            except:
                self.entry.delete(0,END)
                self.entry.insert(END, 'Error')

        #Radians
        elif key in 'rad':
            try:
                self.startOfNextOperand = True
                temp_res = eval(float('radians{}'.format(self.entry.get())))
                self.entry.delete(0,END)
                self.entry.insert(END, temp_res)
            except:
                self.entry.delete(0,END)
                self.entry.insert(END, 'Error')

        #Standard base-n conversion number
        elif key in ['bin','hex','oct','int']:
            try:
                self.startOfNextOperand = True
                temp_res = eval('{} ({})'.format(key, self.entry.get()))
                self.entry.delete(0,END)
                self.entry.insert(END, temp_res)
            except:
                self.entry.delete(0,END)
                self.entry.insert(END, 'Error')

        #Memory Add
        elif key == 'M+':      
            try:
                self.memory = str(eval(self.memory + '+' + self.entry.get()))
            except:
                self.entry.delete(0,END)
                self.entry.insert(END, 'Error')

        #Memory subtract (Subtracting the content of 'self memory variable with the number in the input)
        elif key == 'M-':
            try:
                self.memory = str(eval(self.memory + '-' + self.entry.get()))
            except:
                self.entry.delete(0,END)
                self.entry.insert(END, 'Error')

        #Memory Recall (Returning the content of 'self.memory' to the input pane)
        elif key == 'MR':
            try:
                self.entry.delete(0, END)
                self.entry.insert(END, self.memory)
            except:
                self.entry.delete(0,END)
                self.entry.insert(END, 'Error')

        #Memory Clear (Removing the content of 'self.memory' variable)
        elif key == 'MC':
            try:
                self.memory = '0'
                self.entry.delete(0,END)
                self.entry.insert(END, self.memory)
            except:
                self.entry.delete(0,END)
                self.entry.insert(END, 'Error')

        #32-bit 2's Complement
        elif key == '2C':
            try:
                subs = self.entry.get()
                if '-' in subs:
                    subs = subs[1:]
                    self.entry.delete(0,END)
                    self.entry.insert(END, format(eval(subs), '#032b'))
                else:
                    temp_res = bin (2**32 - eval(self.entry.get()))
                    self.entry.delete(0,END)
                    self.entry.insert(END, format(eval(temp_res), '#032b'))
            except:
                self.entry.delete(0,END)
                self.entry.insert(END, 'Error')

        #Decimal to IEEE 754 
        elif key == '→IEEE':
            try:
                subs = ''
                x= struct.pack('>d', eval(self.entry.get()))
                for j in x: subs += '{:02x}'.format(j)
                self.entry.delete(0,END)
                self.entry.insert(END, subs)
                self.startOfNextOperand = True
            except:
                self.entry.delete(0,END)
                self.entry.insert(END, 'Error')

        #IEEE 754 to Decimal
        elif key == '←IEEE':
            try:
                subs = struct.unpack('>d', struct.pack('>q', int('0x' + self.entry.get(), 16)))[0]
                self.entry.delete(0,END)
                self.entry.insert(END, subs)
                self.startOfNextOperand = True
            except:
                self.entry.delete(0,END)
                self.entry.insert(END, 'Error')
            
        #Condition when numeric input (0-9) and alphabet input (a-e) is clicked            
        else:
            #StartOfNextOperand: When a button is clicked, clears the input pane and input the clicked button
            if self.startOfNextOperand:
                self.entry.delete(0,END)
                self.startOfNextOperand=False
            else:
                pass
            self.entry.insert(END, key)

if __name__ == "__main__":
    Calc()
                   
